import numpy as np



# SET SOME VARS FROM THE REPORT ON THE TEST
FAR = 0.0164
MDR = 0.5882

PRIOR = [0.01, 0.04, 0.07, 0.10]
#PRIOR = [0.001, 0.01, 0.02, 0.05]


# def compute_negative(p):
#     #post = (p * MDR) / ((1.0 - FAR)*(1.0 - p) + (p * MDR))
#     num = MDR * p
#     den = (FAR * (1-p)) + (MDR * p)
#     return num / den

# def compute_positive(p):
#     #print("P: ",p)
#     num = p * (1.0 - MDR)
#     den = FAR * (1.0 - p) + (p * (1.0 - MDR))
#     post = num / den
#     return post

# def compute_mult_pos(p, tests):
#     num = p * (1.0 - MDR)**tests
#     den = ((1-p) * FAR**tests) + (p * (1.0 - MDR)**tests)
#     return num / den


def compute_positive(p, tests):
    num = p * (1.0 - MDR)**tests
    den = ((1-p) * FAR**tests) + (p * (1.0 - MDR)**tests)
    return num / den

def compute_negative(p, tests):
    #post = (p * MDR) / ((1.0 - FAR)*(1.0 - p) + (p * MDR))
    num = (MDR**tests) * p
    den = ((1-FAR)**tests * (1-p)) + (MDR**tests * p)
    return num / den


if __name__ == "__main__":


    for i in PRIOR:
        #c = compute_negative(p=i)
        x = compute_negative(p=i, tests=3)
        print(i,',',x)



 #   print("NEGATIVE CONFIDENCES: ", negatives)
 #   print("POSITIVE CONFIDENCES:", positives)
 #   print("MULT POS CONFIDENCES:", mult_pos)
 #   print("THREE POS CONFIDENCES:", thre_pos)