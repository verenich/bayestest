# Bayestest Prototype

### To run the prototype:

- install Python 3.6 - 3.8 if not installed
- install PIP or PIP3 if not installed
- clone the bayestest repository locally
- cd to code directory

### Now we install the streamlit library:


`pip install streamlit`

### After install run:

`streamlit run bayestest.py`

### You should see something similar to:

`You can now view your streamlit app in your browser.`

`Local URL: http://localhost:8501'

### Go to the URL, should see bayestest running


![alt text](img/bayestest.png "Title Text")
