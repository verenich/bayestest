import streamlit as st
import pandas as pd


st.write("""
# Probability of COVID-19 Positive Given Test""")

DATA_SOURCE = "https://raw.githubusercontent.com/nychealth/coronavirus-data/master/latest/now-tests.csv"
SUM_SOURCE = "https://raw.githubusercontent.com/nychealth/coronavirus-data/master/latest/now-summary.csv"
LAST_REPORT = None
LAST_PRIOR = None
FAR = 0.01 # defaults, these get computed based on imported test characteristics
MDR = 0.02

def load_data():
    data = pd.read_csv(DATA_SOURCE)
    filtered = data["PERCENT_POSITIVE_7DAYS_AVG"]
    LAST_PRIOR = data["PERCENT_POSITIVE_7DAYS_AVG"].iloc[-1]
    LAST_REPORT = data["DATE"].iloc[-1]
    summary = pd.read_csv(SUM_SOURCE)
    test_info = pd.read_csv("data/sofia_test.csv")
    
    return filtered, LAST_REPORT, LAST_PRIOR, summary, test_info

def compute_positive(p, tests):
    num = p * (1.0 - MDR)**tests
    den = ((1-p) * FAR**tests) + (p * (1.0 - MDR)**tests)
    return num / den

def compute_negative(p, tests):
    #post = (p * MDR) / ((1.0 - FAR)*(1.0 - p) + (p * MDR))
    num = (MDR**tests) * p
    den = ((1-FAR)**tests * (1-p)) + (MDR**tests * p)
    return num / den

# read test information and compute FAR and MDR


df, LAST_REPORT, LAST_PRIOR, SUMMARY, test_info = load_data()



test_slider = st.sidebar.slider('Tests Applied', 1,5)
result = st.sidebar.radio("Test Result",('Negative', 'Positive'))
#choices = test_info['SYMPTOMS'].values
symptoms = st.sidebar.radio("Symptoms Present", test_info['SYMPTOMS'].values)
st.text('Lastest Report Date: %s' % LAST_REPORT)
st.text('Lastest Infection Rate: %s' % LAST_PRIOR)



st.write('Tests Used: %s' % test_slider)

# compute proper MDR and FAR based on symptoms choice
row = test_info[test_info.SYMPTOMS == symptoms].values
TP = row[0][1]
TN = row[0][2]
FP = row[0][3]
FN = row[0][4]

FAR = FP / (FP + TN)
MDR = FN / (FN + TP)


st.sidebar.text('Test FAR: %s' % FAR)
st.sidebar.text('Test MDR: %s' % MDR)

post = 0.0
if result == 'Positive':
    post = compute_positive(p=LAST_PRIOR, tests=test_slider)
else:
    post = compute_negative(p=LAST_PRIOR, tests=test_slider)

st.write("""
# %s """ % post)

st.line_chart(df)
st.write(SUMMARY)



